﻿using Application.Queries;
using Application.Queries.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Teste_de_lógica_02.Controllers
{
    [ApiController]
    public class PaymentController : ControllerBase
    {
        // Endpoint de pagamentos
        [HttpGet, Route("Payments")]
        public PaymentResponse GetPaymentsByUserId([FromServices] IMediator mediator, [FromQuery] GetPaymentRequest paymentRequest)
        {
            try
            {
                // Envia o requisição de pagamento para ser executado no Handle
                return mediator.Send(paymentRequest).Result;
            }
            catch (Exception ex)
            {
                var response = new PaymentResponse()
                {
                    Exception = ex.Message
                };

                return response;
            }
        }

    }
}
