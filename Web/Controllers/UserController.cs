﻿using Application.Queries.Responses;
using Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Teste_de_lógica_02.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        // Endpoint de usuarios
        [HttpGet, Route("Users")]
        public UserResponse GetUser([FromServices] IMediator mediator, [FromQuery] GetUserRequest userRequest)
        {
            try
            {
                // return new UserResponse();
                return mediator.Send(userRequest).Result;
            }
            catch(Exception ex)
            {
                return new UserResponse { };
            }
        }

    }
}
