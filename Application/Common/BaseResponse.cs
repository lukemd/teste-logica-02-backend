﻿using Application.Queries.Responses;

namespace Application.Common
{
    public abstract class BaseResponse
    {
        public Status Status { get; set; }
        public string? Exception { get; set; }
    }
}
