﻿using Application.Queries.Responses;
using AutoMapper;

namespace Application.Common.Mappings
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<Domain.Entities.User, User>();
        }
    }
}
