﻿using Application.Queries.Responses;
using AutoMapper;

namespace Application.Common.Mappings
{
    public class PaymentProfile : Profile
    {
        public PaymentProfile() 
        {
            CreateMap<Domain.Entities.Payment, Payment>();
        }

    }
}
