﻿using Application.Queries.Responses;
using MediatR;

namespace Application.Queries
{
    public class GetUserRequest : IRequest<UserResponse>
    {
        public int? Id { get; set; }
        public string? Name { get; set; }
    }
}
