﻿using Application.Queries.Responses;
using MediatR;

namespace Application.Queries
{
    public class GetPaymentRequest : IRequest<PaymentResponse>
    {
        public int? Id { get; set; }
        public string? Name { get; set; }
    }
}
