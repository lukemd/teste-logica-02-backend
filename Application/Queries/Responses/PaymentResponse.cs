﻿using Application.Common;

namespace Application.Queries.Responses
{
    public class PaymentResponse : BaseResponse
    {
        public PaymentResponse()
        {
            Payments = new List<List<Payment>>() { };
        }
        public List<List<Payment>>? Payments { get; set; }
    }

    public class Payment
    {
        public int? Id { get; set; }
        public string? Type { get; set; }
        public double? Value { get; set; }

    }

    public enum Status
    {
        Success = 200,
        Error,
        InvalidUser
    }
}
