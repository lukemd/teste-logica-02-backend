﻿using Application.Common;

namespace Application.Queries.Responses
{
    public class UserResponse : BaseResponse
    {
        public List<User>? users { get; set; }
    }

    public class User
    {
        public int? Id { get; set; }
        public string? Name { get; set; }
    }
}
