﻿using Application.Queries.Responses;
using AutoMapper;
using Domain.Repositories;
using MediatR;

namespace Application.Queries.Handlers
{
    public class GetPaymentsHandler : IRequestHandler<GetPaymentRequest, PaymentResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IMapper _mapper;

        public GetPaymentsHandler(IUserRepository userRepository, IPaymentRepository paymentRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _paymentRepository = paymentRepository;
            _mapper = mapper;
        }

        // Método criado para executar as regras
        public Task<PaymentResponse> Handle(GetPaymentRequest request, CancellationToken cancellationToken)
        {
            // Obtém o tipo de ordenação pelo Id ou Nome do usuário
            var sortType = _userRepository.GetSortTypesByIdOrName(request.Name, request.Id);

            if (sortType == null)
                throw new Exception("Usuário inválido!");

            // Lê o arquivo em excel e ordena a saída conforme o sortType
            var paymentsList = _paymentRepository.GetPaymentsBySortType(sortType);

            // Le a lista e transforma a lista de objetos da camada de dominio na lista de objeto da camada de application   
            var mappedPaymentList = _mapper.Map<List<List<Payment>>>(paymentsList);

            // Map sem usar o automapper
            // var paymentResultList = paymentsList.Select(x => x.Select(y => new Payment() { Id = y.Id, Type = y.Type, Value = y.Value }).ToList()).ToList();

            PaymentResponse paymentResponse = new PaymentResponse()
            {
                Payments = mappedPaymentList,
                Status = Status.Success
            };

            // Retorna o resultado ordenado de pagamentos
            return Task.FromResult(paymentResponse);
        }
    }
}
