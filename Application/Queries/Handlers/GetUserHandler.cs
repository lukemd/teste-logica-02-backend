﻿using Application.Queries.Responses;
using AutoMapper;
using Domain.Repositories;
using MediatR;

namespace Application.Queries.Handlers
{
    public class GetUserHandler : IRequestHandler<GetUserRequest, UserResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public GetUserHandler(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public Task<UserResponse> Handle(GetUserRequest request, CancellationToken cancellationToken)
        {
            var userslist = _userRepository.GetUsers();

            var mappedUserList = _mapper.Map<List<User>>(userslist).ToList();

            var usersResponse = new UserResponse()
            {
                users = mappedUserList,
            };

            return Task.FromResult(usersResponse);
        }
    }
}
