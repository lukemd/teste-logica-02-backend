﻿using Domain.Entities;
using Domain.Repositories;
using OfficeOpenXml;

namespace Infrastructure.Repository
{
    public class PaymentRepository : IPaymentRepository
    {
        public List<List<Payment>> GetPaymentsBySortType(List<string> sortType)
        {   
            // Cria um dicionário para adicionar os pagamentos
            var paymentsDict = new Dictionary<string, List<Payment>>();

            var teste = System.IO.Directory.GetCurrentDirectory();

            // Instancia a classe com o local do arquivo
            FileInfo file = new FileInfo("../payments.xlsm");

            // Biblioteca usada para cria uma nova instância do pacote podendo assim ler o arquivo em excel em código
            using (var package = new ExcelPackage(file))
            {
                // Obtém a primeira tabela
                var worksheet = package.Workbook.Worksheets[0];

                // Obtém o numero total de linhas
                int totalRows = worksheet.Dimension.Rows;

                // Loop para percorrer as linhas e colunas da planilha
                for (int rows = 2; rows <= totalRows; rows++)
                {
                    // Objeto para ser adicionado ao dicionário de pagamentos
                    Payment payment = new Payment();

                    // Tenta fazer a conversão das células para números inteiros e decimais
                    int.TryParse(worksheet.Cells[rows, 1].Text, out int id);
                    double.TryParse(worksheet.Cells[rows, 3].Text, out double value);

                    // Popula as propriedades ao objeto
                    payment.Id = id;
                    payment.Type = worksheet.Cells[rows, 2]?.Text;
                    payment.Value = value;

                    // Verifica se o dicionário já contém a chave
                    if (paymentsDict.ContainsKey(payment.Type))
                        paymentsDict[payment.Type].Add(payment);

                    // Caso não tenha a chave adiciona ao dicionário
                    else
                    {
                        List<Payment> list = new()
                        {
                            payment
                        };
                        paymentsDict[payment.Type.ToString()] = list;
                    }
                }
            }

            // Ordena o dicionário usando a lista de sortType, seleciona apenas os valores de cada chave
            // Converte em lista e adiciona os valores ao objeto de retorno
            var paymentsResultList = paymentsDict.OrderBy(x => sortType.IndexOf(x.Key)).Select(x => x.Value).ToList();
            return paymentsResultList;
        }
    }
}
