﻿using Domain.Entities;
using Domain.Repositories;

namespace Infrastructure.Repository
{
    public class UserRepository : IUserRepository
    {
        public List<string> GetSortTypesByIdOrName(string? name, int? id)
        {
            var user = string.IsNullOrEmpty(name) ? id.ToString() : name;

            // Dado um número ou nome do usuário retorna o tipo de ordenação a ser seguida
            if (user.Equals("alberto quintero") || user.Equals("0"))
                return new List<string> { "Luz", "Água", "Internet" };

            else if (user.Equals("alicia cardin") || user.Equals("1"))
                return new List<string> { "Água", "Internet", "Luz" };

            else if (user.Equals("amandio igrejas") || user.Equals("2"))
                return new List<string> { "Internet", "Luz", "Água" };

            else
                return null;
        }

        public List<User> GetUsers()
        {
            var userList = new List<string>() { "Alberto Quintero", "Alicia Cardin", "Amandio Igrejas" };

            return userList.Select((x, index) => new User() { Id = index, Name = x.ToString() }).ToList();
        }
    }
}
