﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IPaymentRepository
    {
        List<List<Payment>> GetPaymentsBySortType(List<string> sortType);
    }
}
