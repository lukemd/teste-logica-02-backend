﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IUserRepository
    {
        List<string> GetSortTypesByIdOrName(string? name, int? id);

        List<User> GetUsers();
    }
}
