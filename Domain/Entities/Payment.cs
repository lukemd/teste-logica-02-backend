﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Payment
    {
        public int? Id { get; set; }
        public string? Type { get; set; }
        public double? Value { get; set; }

    }
}
